package com.mustaq.mywork.constant

open class AppConstants {
    companion object {
        const val REQUEST_CODE_CREATE_POST = 1
        const val REQUEST_CODE_EDIT_POST = 2
        const val REQUEST_CODE_CAMERA = 3
        const val REQUEST_CODE_GALLERY = 4
        const val IMAGES_CORNER_RADIUS_DP = 8
        const val REQUEST_PERMISSION_CAMERA = 1
        const val REQUEST_PERMISSION_GALLERY = 2
    }
}