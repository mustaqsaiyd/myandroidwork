package com.mustaq.mywork.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.model.EmployeeModel
import com.mustaq.mywork.retrofitClient.RetrofitClientSingleton
import com.mustaq.mywork.viewmodel.DataBindingViewModel
import com.mustaq.mywork.adapter.RecyclerViewAdapter
import com.mustaq.mywork.utils.CustomProgressBar
import com.mustaq.mywork.utils.alertDialog
import retrofit2.Call
import retrofit2.Response

class DataBindingFragment : Fragment() {


    private lateinit var viewModel: DataBindingViewModel
    private lateinit var recyclerView: RecyclerView
    lateinit var customProgressBar: CustomProgressBar
    lateinit var linearLayoutManager: LinearLayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mView = inflater.inflate(R.layout.data_binding_fragment, container, false)
        recyclerView = mView.findViewById(R.id.rvDataBinding)
        getUserList()
        return mView
    }


    private fun getUserList() {
        RetrofitClientSingleton
            .getInstance()
            .getUserList()
            .enqueue(object : retrofit2.Callback<EmployeeModel> {
                override fun onFailure(call: Call<EmployeeModel>, t: Throwable) {
                    Log.e("TAG", t.message!!)
                }

                override fun onResponse(
                    call: Call<EmployeeModel>,
                    response: Response<EmployeeModel>
                ) {
                    if (response.isSuccessful) {
                        if (response.body()!!.status.equals("success")) {
                            if (response.body()!!.data != null) {
                                val employeeList = response.body()!!.data
                                if (employeeList!!.isNotEmpty()) {
                                    linearLayoutManager = LinearLayoutManager(
                                        activity,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
                                    recyclerView.layoutManager = linearLayoutManager
                                    recyclerView.adapter =
                                        RecyclerViewAdapter(
                                            employeeList
                                        )
                                } else {
                                    alertDialog(activity!!, "No Data Found")
                                }

                            } else {
                                alertDialog(activity!!, "Something went wrong")
                            }

                        } else {
                            alertDialog(activity!!, "No Data Found")
                        }
                    }
                }
            })
    }


}
