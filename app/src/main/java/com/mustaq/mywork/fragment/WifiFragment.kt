package com.mustaq.mywork.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mustaq.mywork.R
import com.mustaq.mywork.mainapp.MainApplication
import com.mustaq.mywork.viewmodel.WifiViewModel


class WifiFragment : Fragment() {

    private lateinit var slideshowViewModel: WifiViewModel
    private val REQUEST_ENABLE_WF = 0

    private var wifiManager: WifiManager? = null
    private var listView: ListView? = null
    private val btnWifiOn: Button? = null
    private val size = 0
    private var results: List<ScanResult>? = null
    private val arrayList: ArrayList<String> = ArrayList()
    private var adapter: ArrayAdapter<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
            ViewModelProviders.of(this).get(WifiViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_wifi, container, false)

        wifiManager = MainApplication.getCurrentActivity().getSystemService(Context.WIFI_SERVICE) as WifiManager
        listView = root.findViewById(R.id.wifiList)


        if (!wifiManager!!.isWifiEnabled) {
            Toast.makeText(
                MainApplication.getCurrentContext(),
                "Turning WiFi ON...",
                Toast.LENGTH_LONG
            ).show()
            wifiManager!!.isWifiEnabled = true
        }

        scanWifi()
        adapter = ArrayAdapter(requireActivity(), android.R.layout.simple_list_item_1, arrayList)
        listView!!.adapter = adapter


        return root
    }
    private fun scanWifi() {
        arrayList.clear()
        wifiManager!!.startScan()
        results=wifiManager!!.scanResults
        for (scanResult in results!!) {
            arrayList.add(scanResult.SSID + " - " + scanResult.capabilities)
            adapter!!.notifyDataSetChanged()
        }

        Toast.makeText(requireActivity(), "Scanning WiFi ...", Toast.LENGTH_SHORT).show()
    }


    val wifiReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            results = wifiManager!!.scanResults;

            for (scanResult in results!!) {
                arrayList.add(scanResult.SSID + " - " + scanResult.capabilities)
                adapter!!.notifyDataSetChanged()
            }
        }
    }
    companion object {
        const val TAG = "Wifi Fragment"
    }

}



