package com.mustaq.mywork.fragment

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothClass
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.AdapterBluetoothList
import com.mustaq.mywork.model.BluetoothModel
import com.mustaq.mywork.viewmodel.BluetoothViewModel
import com.mustaq.mywork.utils.CustomProgressBar


class BluetoothFragment : Fragment() {

    private lateinit var galleryViewModel: BluetoothViewModel
    lateinit var customProgressBar: CustomProgressBar

    private val REQUEST_ENABLE_BT = 0
    private val REQUEST_DISCOVERABLE_BT = 0
    lateinit var rvBlutooth: RecyclerView
    lateinit var mAdapter: AdapterBluetoothList
    lateinit var linearLayoutManager: LinearLayoutManager


    private val bAdapter = BluetoothAdapter.getDefaultAdapter()
    val pairedDevices = bAdapter.bondedDevices
    val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(BluetoothViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_bluetooth, container, false)
        val out = root.findViewById<TextView>(R.id.out)
        val turnOn = root.findViewById<AppCompatTextView>(R.id.button1)
        val discover = root.findViewById<AppCompatTextView>(R.id.button2)
        val turnOff = root.findViewById<AppCompatTextView>(R.id.button3)

        rvBlutooth = root.findViewById(R.id.rvBluetooth)
        linearLayoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        rvBlutooth.layoutManager = linearLayoutManager





        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.BLUETOOTH
            ) != PackageManager.PERMISSION_GRANTED
        ) {

        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN
                ),
                REQUEST_ENABLE_BT
            )
        }
        getBluetoothList()


        if (mBluetoothAdapter == null) {
            out.text = "Device not supported"
        } else {
            out.text = "Device  supported"
        }

        turnOn.setOnClickListener {
            if (!mBluetoothAdapter.isEnabled) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
        }

        discover.setOnClickListener {
            if (!mBluetoothAdapter.isEnabled) {
                enableBluetooth()
            } else {
                getBluetoothList()
            }
        }

        turnOff.setOnClickListener {
            mBluetoothAdapter.disable()
            out.text = "TURN_OFF BLUETOOTH"
            Toast.makeText(
                requireActivity(),
                "TURNING_OFF BLUETOOTH",
                Toast.LENGTH_LONG
            )
            mAdapter.clear()

        }
        return root
    }

    private fun getBluetoothList() {

        if (mBluetoothAdapter.isEnabled) {
            val list = ArrayList<BluetoothModel>()
            if (mBluetoothAdapter.isEnabled) {
                if (pairedDevices.size > 0) {
                    for (device in pairedDevices) {
                        val gson = Gson().toJson(device)
                        val bluetoothClass: BluetoothClass = device.bluetoothClass
                        Log.e(TAG, "${bluetoothClass.deviceClass}")
                        var bluetoothModel = BluetoothModel()
                        bluetoothModel.setbName(device.name)
                        bluetoothModel.setbAddress(device.address)
                        Log.e("TAG", "" + device.name + device.type)
                        bluetoothModel.setbType(device.type.toString())
                        bluetoothModel.setbBounds(device.bondState.toString())
                        bluetoothModel.setbClass(bluetoothClass.deviceClass)
                        list.add(bluetoothModel)
                    }

                }
                mAdapter = AdapterBluetoothList(requireActivity(), list)
                rvBlutooth.adapter = mAdapter
            }
        } else {
            enableBluetooth()
        }
    }

    private fun enableBluetooth() {
        Toast.makeText(requireActivity(), "Please Turn on Bluetooth", Toast.LENGTH_SHORT).show()
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
    }

    fun enabledDisableBT() {

        if (mBluetoothAdapter == null) {
            Log.e("TAG", "Device Not Having Bluethooth")
        }
        if (!mBluetoothAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableIntent)
        }
    }


    companion object {
        const val TAG = "BluetoothFragment"
    }
}