package com.mustaq.mywork.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mustaq.mywork.*
import com.mustaq.mywork.activity.*
import com.mustaq.mywork.activity.MainActivity.Companion.TAG
import com.mustaq.mywork.camera.MultipleImageGet
import com.mustaq.mywork.activity.FilterActivity
import com.mustaq.mywork.viewmodel.FirebaseViewModel
import com.mustaq.mywork.workmanager.WorkManagerActivity
import kotlinx.android.synthetic.main.fragment_firebase.*

class FirebaseFragment : Fragment(), View.OnClickListener {

    val fragment = "Firebase Fragment"
    private lateinit var firebaseViewModel: FirebaseViewModel
    lateinit var btnDatabase: AppCompatButton
    lateinit var btnfaceDetectionActivity: AppCompatButton
    lateinit var btnFaceBookSignIn: AppCompatButton
    lateinit var btnGoogleSignIn: AppCompatButton
    lateinit var btnBroadCast: AppCompatButton
    lateinit var btnIntentService: AppCompatButton
    lateinit var btnIntentShareApp: AppCompatButton
    lateinit var btnMultipleImageActivity: AppCompatButton
    lateinit var btnFilterActivity: AppCompatButton
    lateinit var btnAnimation: AppCompatButton
    lateinit var btnWorkManager: AppCompatButton
    lateinit var btnCameraX: AppCompatButton
    lateinit var buttonAnimation: AppCompatButton
    lateinit var buttonSignUp: AppCompatButton
    lateinit var btnGestureDetectors: AppCompatButton
    lateinit var btnNotification: AppCompatButton
    lateinit var btnPermissionActivityQ: AppCompatButton
    lateinit var btnAndroidViewPager2: AppCompatButton
    lateinit var btnSensors: AppCompatButton
    lateinit var btnEndlessRecyclerView: AppCompatButton

    override fun onAttach(context: Context) {
        Log.d(TAG, "onAttach: $fragment")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate: $fragment")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: $fragment")
        firebaseViewModel = ViewModelProviders.of(this).get(FirebaseViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_firebase, container, false)
        btnDatabase = root.findViewById(R.id.btnDatabase)
        btnFaceBookSignIn = root.findViewById(R.id.btnFaceBookSignIn)
        btnGoogleSignIn = root.findViewById(R.id.btnGoogleSignIn)
        btnBroadCast = root.findViewById(R.id.btnBroadCast)
        btnIntentService = root.findViewById(R.id.btnIntentService)
        btnIntentShareApp = root.findViewById(R.id.btnIntentShareApp)
        btnMultipleImageActivity = root.findViewById(R.id.btnMultipleImageActivity)
        btnFilterActivity = root.findViewById(R.id.btnFilterActivity)
        btnAnimation = root.findViewById(R.id.btnAnimation)
        btnWorkManager = root.findViewById(R.id.btnWorkManager)
        btnCameraX = root.findViewById(R.id.btnCameraX)
        btnfaceDetectionActivity = root.findViewById(R.id.faceDetectionActivity)
        buttonAnimation = root.findViewById(R.id.buttonAnimation)
        buttonSignUp = root.findViewById(R.id.buttonSignUp)
        btnGestureDetectors = root.findViewById(R.id.btnGestureDetectors)
        btnNotification = root.findViewById(R.id.btnNotification)
        btnPermissionActivityQ = root.findViewById(R.id.btnPermissionActivityQ)
        btnAndroidViewPager2 = root.findViewById(R.id.btnAndroidViewPager2)
        btnSensors = root.findViewById(R.id.btnSensors)
        btnEndlessRecyclerView = root.findViewById(R.id.btnEndlessRecyclerView)



        btnDatabase.setOnClickListener {
            startActivity(Intent(requireActivity(), FirebaseDatabaseActivity::class.java))
        }
        btnfaceDetectionActivity.setOnClickListener {
            startActivity(Intent(requireActivity(), FaceDetectionActivity::class.java))
        }
        btnFaceBookSignIn.setOnClickListener {
            startActivity(Intent(requireActivity(), SocialLoginFirebaseActivity::class.java))
        }

        btnBroadCast.setOnClickListener {
            startActivity(Intent(requireActivity(), BroadCastExampleActivity::class.java))
        }


        buttonSignUp.setOnClickListener {
            startActivity(Intent(requireActivity(), SignupActivity::class.java))
        }
        btnIntentShareApp.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            /* val title=resources.getString(R.string.chooser_title)
             val chooser=Intent.createChooser(intent,title)
             startActivity(chooser)*/
        }

        btnAnimation.setOnClickListener {
            startActivity(Intent(requireActivity(), AnimationActivity::class.java))
        }

        btnMultipleImageActivity.setOnClickListener {
            startActivity(Intent(requireActivity(), MultipleImageGet::class.java))
        }

        btnFilterActivity.setOnClickListener {
            startActivity(Intent(requireActivity(), FilterActivity::class.java))
        }
        btnWorkManager.setOnClickListener {
            startActivity(Intent(requireActivity(), WorkManagerActivity::class.java))
        }
        buttonAnimation.setOnClickListener {
            startActivity(Intent(requireActivity(), MotionLayoutActivity::class.java))
        }

        btnCameraX.setOnClickListener {
            startActivity(Intent(requireActivity(), CameraXActivity::class.java))
        }
        btnGestureDetectors.setOnClickListener {
            startActivity(Intent(requireActivity(), GestureDetectorsActivity::class.java))
        }
        btnNotification.setOnClickListener {
            startActivity(Intent(requireActivity(), AndroidNotificationsActivity::class.java))
        }
        btnPermissionActivityQ.setOnClickListener {
            startActivity(Intent(requireActivity(), PermissionInQActivity::class.java))
        }
        btnAndroidViewPager2.setOnClickListener {
            startActivity(Intent(requireActivity(), AndroidViewPagerActivity::class.java))
        }
        btnSensors.setOnClickListener {
            startActivity(Intent(requireActivity(), SansorsActivity::class.java))
        }
        btnEndlessRecyclerView.setOnClickListener {
            startActivity(Intent(requireActivity(), EndlessRecyclerViewActivity::class.java))
        }


        return root
    }


    override fun onClick(v: View?) {
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.d(TAG, "onActivityCreated: $fragment")
        super.onActivityCreated(savedInstanceState)
    }

    override fun onAttachFragment(childFragment: Fragment) {
        Log.d(TAG, "onAttachFragment: $fragment")
        super.onAttachFragment(childFragment)
    }

    override fun onStart() {
        Log.d(TAG, "onStart: $fragment")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume: $fragment")
        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "onPause: $fragment")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop: $fragment")
        super.onStop()
    }

    override fun onDetach() {
        Log.d(TAG, "onDetach: $fragment")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy: $fragment")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.d(TAG, "onDestroyView: $fragment")
        super.onDestroyView()
    }


}