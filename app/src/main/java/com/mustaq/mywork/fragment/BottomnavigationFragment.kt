package com.mustaq.mywork.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.badge.BadgeDrawable
import com.mustaq.mywork.R
import com.mustaq.mywork.viewmodel.BottomnavigationViewModel
import kotlinx.android.synthetic.main.fragment_bottomnavigation.view.*


class BottomnavigationFragment : Fragment() {

    private lateinit var toolsViewModel: BottomnavigationViewModel


    var animFadein: Animation? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        toolsViewModel =
            ViewModelProviders.of(this).get(BottomnavigationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_bottomnavigation, container, false)

        val badge: BadgeDrawable = root.navigation.getOrCreateBadge(0)
        badge.isVisible = true
        return root
    }
}