package com.mustaq.mywork.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.AdapterFakeCallApi
import com.mustaq.mywork.retrofitClient.ServiceGenerator
import com.mustaq.mywork.utils.centerToast
import com.mustaq.mywork.viewmodel.FakeApiModel
import com.mustaq.mywork.viewmodel.FakeApiViewModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class FakeApiCallFragment : Fragment() {


    private lateinit var viewModel: FakeApiViewModel
    lateinit var recyclerView: RecyclerView
    lateinit var adapterFakeCallApi: AdapterFakeCallApi

    val user = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val mView = inflater.inflate(R.layout.fack_api_fragment, container, false)

        recyclerView = mView.findViewById(R.id.rvList)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        setHasOptionsMenu(true)
        getApi()
        return mView
    }


    fun getApi() {
        val apiCall = "https://reqres.in/api/"
        val retrofit = Retrofit.Builder()
            .baseUrl(apiCall)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val apiHelper = retrofit.create(ServiceGenerator::class.java)
        val call = apiHelper.getFakeCall()
        call.enqueue(object : retrofit2.Callback<List<FakeApiModel.DataBean>> {
            override fun onFailure(call: Call<List<FakeApiModel.DataBean>>, t: Throwable) {
                Log.d("TAG", "onFailure: $t")
                centerToast("$t")
            }

            override fun onResponse(
                call: Call<List<FakeApiModel.DataBean>>,
                response: Response<List<FakeApiModel.DataBean>>
            ) {
                if (response.isSuccessful) {
                    Log.e("TAG", "onResponse: True" + response.code())
                    val respo = response.body()

                    Log.e(TAG, "onResponse: ${respo!!.size}" )
                    /*val mAdapter = AdapterFakeCallApi(
                        requireActivity(),
                        respo
                    )
                    recyclerView.adapter = mAdapter*/
                } else {
                    Log.e("TAG", "onResponse: False" + response.code())
                }
            }

        })


    }
    companion object{
        const val TAG="Fake Api Call Fragment"
    }

}

/**
 *  val respo = response.body()
val mAdapter = AdapterFackCallApi(requireActivity(), respo)
recyclerView.adapter = mAdapter**/