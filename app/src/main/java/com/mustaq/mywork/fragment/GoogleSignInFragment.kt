package com.mustaq.mywork.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.mustaq.mywork.R
import com.mustaq.roomexample.ui.googlesignin.GoogleSignInViewModel
import kotlinx.android.synthetic.main.fragment_google_sign_in.*

class GoogleSignInFragment : Fragment(), View.OnClickListener {



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_google_sign_in, container, false)

        // Button listeners
        val signInButton = root.findViewById<Button>(R.id.signInButton)
        val signOutButton = root.findViewById<Button>(R.id.signOutButton)
        val disconnectButton = root.findViewById<Button>(R.id.disconnectButton)

        signInButton.setOnClickListener(this)
        signOutButton.setOnClickListener(this)
        disconnectButton.setOnClickListener(this)

        return root
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.signInButton -> signIn()
            R.id.signOutButton -> signOut()
            R.id.disconnectButton -> revokeAccess()
        }
    }

    private fun revokeAccess() {

    }

    private fun signOut() {
    }

    private fun signIn() {
    }

    companion object {
        private const val RC_SIGN_IN = 9001
    }


}