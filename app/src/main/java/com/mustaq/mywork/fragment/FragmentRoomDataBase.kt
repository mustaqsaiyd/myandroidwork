package com.mustaq.mywork.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.offline.ui.OfflineRoomApiCallActivity
import com.mustaq.mywork.offline.ui.RoomDatabaseActivity
import com.mustaq.mywork.viewmodel.RoomDataBaseViewModel
import kotlinx.android.synthetic.main.room_data_base_fragment.view.*


/**link
 * https://www.youtube.com/watch?v=CTBiwKlO5IU
 **/
class FragmentRoomDataBase : Fragment() {

    lateinit var recyclerView: RecyclerView

    companion object {
        const val TAG = "FRAGMENTROOMDATABASE"
    }

    private lateinit var viewModel: RoomDataBaseViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.room_data_base_fragment, container, false)


        root.btnOfflineApi.setOnClickListener {
            startActivity(Intent(requireActivity(), OfflineRoomApiCallActivity::class.java))
        }

        root.btnRoomDatabase.setOnClickListener {
            startActivity(Intent(requireActivity(), RoomDatabaseActivity::class.java))
        }
        return root
    }


}
