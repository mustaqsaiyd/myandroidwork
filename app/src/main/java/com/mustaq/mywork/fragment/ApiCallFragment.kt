package com.mustaq.mywork.fragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.viewmodel.ApiCallViewModel


class ApiCallFragment : Fragment() {
    private val mViewModel: ApiCallViewModel? = null
    lateinit var recyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val apiCallFragment = inflater.inflate(R.layout.api_call_fragment, container, false)
        recyclerView = apiCallFragment.findViewById(R.id.rvUserList)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        setHasOptionsMenu(true)
        return apiCallFragment
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

}