package com.mustaq.mywork.fragment


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.activity.MainActivity.Companion.TAG
import com.mustaq.mywork.R
import com.mustaq.mywork.activity.DetectFacesActivity
import com.mustaq.mywork.adapter.GithubAdapter
import com.mustaq.mywork.helper.UserPermissionRequest.hasGranted
import com.mustaq.mywork.helper.UserPermissionRequest.hasSelfPermissions
import com.mustaq.mywork.retrofitClient.ServiceGenerator
import com.mustaq.mywork.retrofitClient.StackApiResponse
import com.mustaq.mywork.viewmodel.PaginationViewModel
import com.mustaq.mywork.utils.userRequestMultiplePermission
import com.mustaq.mywork.utils.CustomProgressBar
import com.mustaq.mywork.utils.centerToast
import kotlinx.android.synthetic.main.fragment_pagination.*
import kotlinx.android.synthetic.main.fragment_pagination.view.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PaginationFragment : Fragment() {

    val fragment = "Pagination Fragment"
    private lateinit var homeViewModel: PaginationViewModel
    lateinit var mAdapter: GithubAdapter
    lateinit var dynamicRecyclerView: RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var gridLayoutManager: GridLayoutManager
    lateinit var customProgressBar: CustomProgressBar
    val PAGE_SIZE = 50

    //we will start from the first page which is 1
    private val FIRST_PAGE = 1

    //we need to fetch from stackoverflow
    private val SITE_NAME = "stackoverflow"
    var isSwitched = true

    private val permissionArray = arrayOf(
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.READ_SMS,
        android.Manifest.permission.CALL_PHONE,
        android.Manifest.permission.READ_CALL_LOG,
        android.Manifest.permission.CAMERA
    )

    override fun onAttach(context: Context) {
        Log.d(TAG, "onAttach: $fragment")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate: $fragment")
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("StringFormatInvalid")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: $fragment")
        homeViewModel =
            ViewModelProviders.of(this).get(PaginationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_pagination, container, false)
        dynamicRecyclerView = root.findViewById(R.id.recyclerView)

        gridLayoutManager = GridLayoutManager(requireActivity(), 2)
        dynamicRecyclerView.setHasFixedSize(true)
        customProgressBar = CustomProgressBar()
        //getGitHubList()
        /* userRequestMultiplePermission(
             requireActivity(),
             permissionArray
         )*/
        val code = hasGranted(PackageManager.PERMISSION_GRANTED)
        val codeArray = hasGranted(PackageManager.PERMISSION_GRANTED)


        root.btnList.setOnClickListener {
            val intent = Intent(requireActivity(), DetectFacesActivity::class.java)
            startActivity(intent)
        }

        /*root.btnGrid.setOnClickListener {
            isSwitched = mAdapter.toggleItemViewType()
            dynamicRecyclerView.layoutManager = gridLayoutManager
            mAdapter.notifyDataSetChanged()
            root.btnGrid.isEnabled = false
            root.btnList.isEnabled = true
        }
        root.btnList.setOnClickListener {
            isSwitched = mAdapter.toggleItemViewType()
            dynamicRecyclerView.layoutManager = linearLayoutManager
            mAdapter.notifyDataSetChanged()
            root.btnGrid.isEnabled = true
            root.btnList.isEnabled = false
        }*/


        /*if (hasSelfPermissions(requireActivity(), permissionArray)){
            userRequestMultiplePermission(
                requireActivity(),
                permissionArray
            )
        }*/

        return root
    }


    fun getGitHubList() {
        customProgressBar.dialogShow(requireActivity())
        val BASE_URL = "https://api.stackexchange.com/2.2/"

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiservice = retrofit.create(ServiceGenerator::class.java)

        val call = apiservice.getAnswers(FIRST_PAGE, PAGE_SIZE, SITE_NAME)
        call!!.enqueue(object : retrofit2.Callback<StackApiResponse?> {
            override fun onFailure(call: Call<StackApiResponse?>, t: Throwable) {
                customProgressBar.hideDialog(activity!!)
            }

            override fun onResponse(
                call: Call<StackApiResponse?>,
                response: Response<StackApiResponse?>
            ) {

                if (response.isSuccessful) {
                    if (response.body()!!.items!!.isNotEmpty()) {
                        customProgressBar.hideDialog(activity!!)
                        linearLayoutManager = LinearLayoutManager(
                            activity,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        dynamicRecyclerView.layoutManager = linearLayoutManager
                        mAdapter = GithubAdapter(activity!!, response)
                        dynamicRecyclerView.adapter = mAdapter
                    } else {
                        centerToast("Something went wrong")
                    }
                } else {
                    centerToast("Data Not Available")
                }
            }

        })

    }
}

//for demo commit in bitbucket
//for demo