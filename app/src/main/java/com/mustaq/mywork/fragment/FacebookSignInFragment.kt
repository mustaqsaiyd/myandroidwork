package com.mustaq.mywork.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mustaq.mywork.R
import com.mustaq.roomexample.ui.facebooksigin.FacebookSiginInViewModel

class FacebookSignInFragment : Fragment() {

    private lateinit var galleryViewModel: FacebookSiginInViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(FacebookSiginInViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_facebook_sign_in, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
        galleryViewModel.text.observe(activity!!, Observer {
            textView.text = it
        })
        return root
    }
}