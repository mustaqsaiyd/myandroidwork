package com.mustaq.mywork.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.PostViewHolder
import com.mustaq.mywork.model.Post
import com.mustaq.mywork.retrofitClient.ImApi
import com.mustaq.mywork.retrofitClient.RetrofitClient.retrofitInstance
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.rx_java_call_fragment.view.*

class RxJavaCallFragment : Fragment() {
    var imApi: ImApi? = null
    var rxRecyclerView: RecyclerView? = null
    var mCompositeDisposable = CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            inflater.inflate(R.layout.rx_java_call_fragment, container, false)
        val retrofit = retrofitInstance
        imApi = retrofit!!.create(ImApi::class.java)
        rxRecyclerView = view.findViewById(R.id.rxRecyclerView)
        view.rxRecyclerView.layoutManager = LinearLayoutManager(
            requireActivity(),
            LinearLayoutManager.VERTICAL,
            false
        )
        fetchData()
        return view
    }

    private fun fetchData() {
        mCompositeDisposable.add(
            imApi!!.post
            !!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { postModels -> displayData(postModels as List<Post>) }
        )
    }

    private fun displayData(employeeModelsList: List<Post>) {
        val postAdapter = PostViewHolder(requireActivity(), employeeModelsList)
        rxRecyclerView!!.adapter = postAdapter
    }

    override fun onStop() {
        mCompositeDisposable.clear()
        super.onStop()
    }
}