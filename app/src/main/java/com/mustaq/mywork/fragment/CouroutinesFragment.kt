package com.mustaq.mywork.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.retrofitClient.ApiHelper
import com.mustaq.mywork.retrofitClient.RetrofitBuilder
import com.mustaq.mywork.model.UserCouroutinModel
import com.mustaq.mywork.model.Status
import com.mustaq.mywork.viewmodelfactory.ViewModelFactory
import com.mustaq.mywork.adapter.UserAdapter
import com.mustaq.mywork.viewmodel.CouroutinesViewModel
import kotlinx.android.synthetic.main.activity_main_couroutines.*
import kotlinx.android.synthetic.main.fragment_pagination.recyclerView

class CouroutinesFragment : Fragment() {

    companion object {
        fun newInstance() = CouroutinesFragment()
    }

    private lateinit var viewModel: CouroutinesViewModel
    private lateinit var adapter: UserAdapter
    private lateinit var mRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.couroutines_fragment, container, false)
        mRecyclerView = view.findViewById(R.id.recyclerView)
        setupViewModel()
        setupUI()
        setupObservers()
        return view
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            requireActivity(),
            ViewModelFactory(
                ApiHelper(
                    RetrofitBuilder.apiService
                )
            )
        ).get(CouroutinesViewModel::class.java)
    }

    private fun setupUI() {
        mRecyclerView.layoutManager = LinearLayoutManager(requireActivity())
        adapter = UserAdapter(arrayListOf())
        mRecyclerView.addItemDecoration(
            DividerItemDecoration(
                mRecyclerView.context,
                (mRecyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        mRecyclerView.adapter = adapter

    }

    private fun setupObservers() {
        viewModel.getUsers().observe(requireActivity(), Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        Log.e("TAG", Status.SUCCESS.toString())
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        resource.data?.let { users -> retrieveList(users) }
                    }
                    Status.ERROR -> {
                        Log.e("TAG", Status.ERROR.toString())
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        Log.e("TAG", Status.LOADING.toString())
                        progressBar.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    }
                }
            }
        })

    }
    private fun retrieveList(userCouroutinModels: List<UserCouroutinModel>) {
        Log.e("TAG", "retrieveList")
        adapter.apply {
            addUsers(userCouroutinModels)
            notifyDataSetChanged()
        }
    }

}
