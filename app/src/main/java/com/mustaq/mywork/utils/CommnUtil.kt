package com.mustaq.mywork.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.Toast
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.FileProvider
import com.mustaq.mywork.BuildConfig
import com.mustaq.mywork.R
import com.mustaq.mywork.helper.UserPermissionRequest
import com.mustaq.mywork.mainapp.MainApplication
import kotlinx.android.synthetic.main.alart_layout.*
import java.io.File

class CommnUtil

const val REQUEST_CODE = 0


fun bottomToast(context: Context, message: String) {
    val toast = Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT)
    toast.show()
}

fun centerToast(context: Context, message: String) {
    val toast = Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun topToast(context: Context, message: String) {
    val toast = Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.TOP, 0, 0)
    toast.show()
}


fun alertDialog(context: Context, message: String) {
    val myDialog = Dialog(context)
    myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    myDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    myDialog.setContentView(R.layout.alert_layout)
    myDialog.tvAlertTitle.text = message
    myDialog.show()

    myDialog.btn_ok.setOnClickListener {
        myDialog.dismiss()
    }
}


/**User Permission function**/
fun userRequestMultiplePermission(context: Activity, permissionArray: Array<String>) {
    requestPermissions(
        context, permissionArray,
        REQUEST_CODE
    )
}


fun hasSelfPermission(context: Context, permission: String): Boolean {
    return if (UserPermissionRequest.isPermisssion) {
        UserPermissionRequest.permissionHasGranted(context, permission)
    } else true
}


fun mToast(context: Context, text: String?) {
    if (context != null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
        toast.show()
    }
}

fun isValidMail(mailString: String): Boolean {
    return !TextUtils.isEmpty(mailString) && android.util.Patterns.EMAIL_ADDRESS.matcher(mailString)
        .matches()
}

fun isValidUserName(str: String): Boolean {
    val expression = "^[a-zA-Z0-9]*$"
    return str.matches(expression.toRegex())
}


fun clearCookies(context: Context) {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
        CookieSyncManager.createInstance(context)
        CookieManager.getInstance().removeAllCookies(null)
        CookieManager.getInstance().flush()
    } else {
        CookieSyncManager.createInstance(context)
        val cookieManager = CookieManager.getInstance()
        cookieManager.removeAllCookie()
    }
}

fun getUriFromFile(context: Context, file: File): Uri? =
    FileProvider.getUriForFile(context, "${BuildConfig.APPLICATION_ID}.provider", file)


fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}


/*fun startActivityWithoutParamater(context: Context, nextActivity: Class<out Activity>) {
    startActivity(Intent(context, nextActivity))
}*/

fun hideKeyboard(activity: Activity) {
    val view = activity.currentFocus
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    val handler = Handler()
    handler.postDelayed({
        if (view == null) {
            val view2 = View(activity)
            imm.hideSoftInputFromWindow(view2.windowToken, 0)
        } else {
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }, 125)
}

fun centerToast(message: String) {
    if (MainApplication.getCurrentActivity() != null) {
        val toast = Toast.makeText(MainApplication.getCurrentContext(), message, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }
}