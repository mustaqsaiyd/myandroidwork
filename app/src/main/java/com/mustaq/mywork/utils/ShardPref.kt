package com.mustaq.mywork.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri


object ShardPref {
    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    fun savedata(context: Context, imageUrl: Uri) {
        val appSharedPrefs = getSharedPreferences(context)
        val prefsEditor = appSharedPrefs.edit()
        prefsEditor.putString("ShardProfileImage", imageUrl.toString())
        prefsEditor.commit()
    }

    fun getProfileShard(context: Context): String =
        getSharedPreferences(context).getString("ShardProfileImage", "")!!

}
