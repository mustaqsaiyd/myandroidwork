package com.mustaq.mywork.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import com.mustaq.mywork.constant.AppConstants
import java.io.File
import java.io.IOException
import java.util.*

abstract class BasePictureActivity : AppCompatActivity() {

    private var tempCameraFile: File? = null
    private fun openCamera() {
        val dir = ContextCompat.getExternalCacheDirs(this)[0].absoluteFile
        if (!dir.isDirectory) dir.mkdir()

        tempCameraFile = File(dir, "${Date().time}.png")
        try {
            if (!tempCameraFile!!.createNewFile()) {
                Log.e("check", "unable to create empty file")
                return
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
            Log.e("check", "unable to create empty file")
            return
        }

        val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        i.putExtra(MediaStore.EXTRA_OUTPUT, getUriFromFile(this, tempCameraFile!!))
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(i, AppConstants.REQUEST_CODE_CAMERA)
    }
    private fun openGallery() {
        val i = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(i, AppConstants.REQUEST_CODE_GALLERY)
    }
    protected fun getPictureFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PermissionChecker.PERMISSION_DENIED
            ) {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    AppConstants.REQUEST_PERMISSION_GALLERY
                )
            } else {
                openGallery()
            }
        } else {
            openGallery()
        }
    }
    protected fun getPictureFromCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) == PermissionChecker.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PermissionChecker.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PermissionChecker.PERMISSION_DENIED
            ) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    AppConstants.REQUEST_PERMISSION_CAMERA
                )
            } else {
                openCamera()
            }
        } else {
            openCamera()
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var granted = true
        for (i in permissions.indices) {
            if (grantResults[i] != PermissionChecker.PERMISSION_GRANTED) {
                granted = false
                break
            }
        }
        if (granted) {
            when (requestCode) {
                AppConstants.REQUEST_PERMISSION_GALLERY -> openGallery()
                AppConstants.REQUEST_PERMISSION_CAMERA -> openCamera()
            }
        } else {
            pictureError("Permissions not granted")
        }
    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == Activity.RESULT_OK) {
                when (requestCode) {
                    AppConstants.REQUEST_CODE_GALLERY -> {
                        Log.e("=====", "Enter in gallery ")
                        val selectedImageUri = data?.getData()
                        if (null != selectedImageUri) {
                            // Get the path from the Uri
                            val path = getPathFromURI(selectedImageUri)
                            // Set the image in ImageView
                            pictureResult(
                                File(path)
                            )
                        }
//                        pictureResult(
//                            File(
//                                GeneralFunctions.getFilePathFromUri(
//                                    this,
//                                    data!!.data
//                                )!!
//                            )
//                        )
                    }
                    AppConstants.REQUEST_CODE_CAMERA -> {
                        if (tempCameraFile != null) {
                            pictureResult(File(tempCameraFile!!.absolutePath))
                        } else {
                            pictureError("Some error occurred")
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    abstract fun pictureResult(file: File)
    abstract fun pictureError(errorMessage: String)
    /* Get the real path from the URI */
    fun getPathFromURI(contentUri: Uri): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(contentUri, proj, null, null, null)
        if (cursor!!.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }
}
