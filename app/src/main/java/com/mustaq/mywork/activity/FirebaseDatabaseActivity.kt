package com.mustaq.mywork.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.mustaq.mywork.R
import com.mustaq.mywork.utils.centerToast
import com.mustaq.mywork.model.User
import kotlinx.android.synthetic.main.activity_create_user.*
import kotlinx.android.synthetic.main.activity_firebase_database.*

class
FirebaseDatabaseActivity : AppCompatActivity() {
    val TAG = "MainActivity"
    val activity = "Firebase Database Activity "
    private lateinit var dbReference: DatabaseReference
    lateinit var saveButton: AppCompatButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_database)

        Log.d(TAG, "onCreate: $activity")
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        dbReference = FirebaseDatabase.getInstance().getReference("users")

        saveButton = findViewById(R.id.save)

        saveButton.setOnClickListener(View.OnClickListener {

            when {
                edName.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter Name"
                    )
                }
                edLastName.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter LastName"
                    )
                }
                edFFMobile.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter Mobile"
                    )
                }
                edFFemail.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter Email"
                    )
                }
                edFFAddress.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter Address"
                    )
                }
                edFFPincode.text!!.isEmpty() -> {
                    centerToast(
                        this,
                        "Please Enter Pincode"
                    )
                }
            }
            createUser(edName.text.toString(), edMobile.text.toString())

        })
    }

    override fun onStart() {
        Log.d(TAG, "onStart: $activity")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume: $activity")
        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "onPause: $activity")
        super.onPause()
    }

    override fun onRestart() {
        Log.d(TAG, "onRestart: $activity")
        super.onRestart()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy: $activity")
        super.onDestroy()
    }

    private fun createUser(name: String, mobile: String) {
        val userId = dbReference.push().key
        val user = User(name, mobile)
        userId?.let { dbReference.child(it).setValue(user) }
    }
}