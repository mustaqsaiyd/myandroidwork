package com.mustaq.mywork.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.mustaq.mywork.R

class FragmentPagerActivity : AppCompatActivity() {

    companion object {
        const val TAG = "FragmentPagerActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_pager)
        Log.d(TAG, "onCreate: Started $TAG")

    }
}
