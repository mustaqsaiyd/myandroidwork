package com.mustaq.mywork.activity

import android.os.Bundle
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.mustaq.mywork.R
import com.mustaq.mywork.utils.BasePictureActivity
import java.io.File

class SignupActivity : BasePictureActivity() {

    lateinit var image: AppCompatImageView
    private var imageFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        image = findViewById(R.id.imgPhoto);


        image.setOnClickListener {
            android.widget.PopupMenu(this, it).apply {
                menuInflater.inflate(R.menu.menu_popup_picture, menu)
                setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.menu_gallery -> {
                            getPictureFromGallery()
                        }
                        R.id.menu_camera -> {
                            getPictureFromCamera()
                        }
                    }
                    true
                }
                show()
            }
        }
    }

    override fun pictureResult(file: File) {
        imageFile = file
        Glide.with(this)
            .load(imageFile!!)
            .transform(CircleCrop())
            .into(image)
    }

    override fun pictureError(errorMessage: String) {
        Glide.with(this).load(R.drawable.android).transform(CircleCrop()).into(image)
    }
}