package com.mustaq.mywork.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.FilterAdapter
import com.mustaq.mywork.model.FilterResponse

class FilterActivity : AppCompatActivity() {

    lateinit var filterAdapter: FilterAdapter
    lateinit var linearlayout: LinearLayout
    lateinit var rvFilter: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        rvFilter=findViewById(R.id.rvFilterColor)

        val colorMap: MutableMap<String, String> = HashMap()
        colorMap["Blue"] = "#0000FF"
        colorMap["Gray"] = "#808080"
        colorMap["Green"] = "#008000"
        colorMap["Yellow"] = "#FFFF00"
        colorMap["Black"] = "#000000"
        colorMap["Salmon"] = "#FA8072"
        colorMap["LightCoral"] = "#F08080"
        colorMap["DarkSalmon"] = "#E9967A"
        colorMap["Olive"] = "#808000"
        colorMap["Lime"] = "#00FF00"
        colorMap["Fuchsia"] = "#FF00FF"
        colorMap["Purple"] = "#800080"
        colorMap["Navy"] = "#000080"




        val filterResponse = FilterResponse()
        filterResponse.color= listOf(colorMap)
        val listColor = filterResponse.color
        val map= mutableMapOf<String,String>()
        for(i in listColor!!.indices){
            for (j in listColor[i].keys){
                map[j] = listColor[i][j]!!
            }

        }

        for (key in filterResponse.color!!.indices) {
            Log.d("TAG", "Element at key $key : ${filterResponse.color!![key]}")
        }

        filterAdapter = FilterAdapter(this, map)

        rvFilter.layoutManager = LinearLayoutManager(this)
        rvFilter.adapter = filterAdapter
    }
}