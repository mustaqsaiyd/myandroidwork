package com.mustaq.mywork.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.mustaq.mywork.R;

public class SecondActivity extends Activity {

    private static final String TAG = "Second Activity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity_layout);

        Log.d(TAG, "onCreate: Started" + TAG);
    }
}
