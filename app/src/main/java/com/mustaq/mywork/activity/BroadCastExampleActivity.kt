package com.mustaq.mywork.activity


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.*
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import com.google.mlkit.vision.face.FaceLandmark
import com.mustaq.mywork.BuildConfig
import com.mustaq.mywork.R
import com.mustaq.mywork.camera.FileCompressor
import com.mustaq.mywork.helper.UserPermissionRequest
import com.mustaq.mywork.reciver.ConnectionReceiver
import com.mustaq.mywork.services.MyService
import com.mustaq.mywork.utils.ShardPref
import kotlinx.android.synthetic.main.activity_broadcast_example.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class BroadCastExampleActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    lateinit var receiver: BroadcastReceiver
    lateinit var serviceIntent: Intent
    lateinit var wifiswitch: Switch
    lateinit var bluetoothSwitch: Switch
    lateinit var wifiManager: WifiManager
    lateinit var buttonChoose: AppCompatButton
    lateinit var imageChoose: AppCompatImageView
    private var bAdapter = BluetoothAdapter.getDefaultAdapter()
    lateinit var tvMore: TextView
    lateinit var tvSpane: TextView
    var mPhotoFile: File? = null
    var mCompressor: FileCompressor? = null
    lateinit var spinner: Spinner
    private var _day = 0
    private var _month = 0
    private var _birthYear = 0
    var normalCount = 0
    var colorChangeCount = 0

    private val mImage: ImageView? = null
    private val mImageUri: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broadcast_example)
        receiver = ConnectionReceiver()

        buttonChoose = findViewById(R.id.btnImgChoose)
        imageChoose = findViewById(R.id.imgPhoto)
        tvMore = findViewById(R.id.tvMore)
        spinner = findViewById(R.id.planets_spinner)
        mCompressor = FileCompressor(this)

        val imageUrl = ShardPref.getProfileShard(this)
        Glide.with(this)
            .load(imageUrl)
            .apply(
                RequestOptions().centerCrop()
                    .circleCrop()
                    .placeholder(R.drawable.ic_launcher_background)
            )
            .into(imageChoose)

        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED)
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED)
        intentFilter.addAction(Intent.ACTION_BATTERY_LOW)
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent!!.action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                    Toast.makeText(
                        applicationContext,
                        "Connection Connected",
                        android.widget.Toast.LENGTH_SHORT
                    ).show()
                }
                if (intent.action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                    Toast.makeText(
                        applicationContext,
                        "Connection Refused",
                        android.widget.Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        registerReceiver(receiver, intentFilter)

        sendBroadcast(Intent("MyReceiver"))




        wifiswitch = findViewById(R.id.wifi_State)
        wifiManager = application!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        serviceIntent = Intent(this, MyService::class.java)
        buttonChoose.setOnClickListener {
            selectImage()
        }

        if (wifiManager.isWifiEnabled) {
            wifiswitch.isChecked = true
            wifiswitch.text = "Wifi on"
        } else {
            wifiswitch.isChecked = false
            wifiswitch.text = "Wifi off"
        }


        wifiswitch.setOnCheckedChangeListener { _, isCheck ->
            if (isCheck) {
                wifiManager.isWifiEnabled = true
                wifiswitch.text = "Wifi On"
            } else {
                wifiManager.isWifiEnabled = false
                wifiswitch.text = "Wifi Off"
            }

        }

        expandableTextView()
        spinnerSet()
        btnImgChoose.tooltipText = "Choose Image"
        btnShowDateTime.setOnClickListener {
            openDateTimeDialog()
        }

        btnNotification.setOnClickListener {


        }

        btnShow.setOnClickListener {
            val st = findViewById<EditText>(R.id.txtMsg)
            val intent = Intent()
            intent.putExtra("msg", st.text.toString() as CharSequence)
            intent.action = "com.mustaq.mywork.CUSTOM_INTENT"
            sendBroadcast(intent)
        }

        serviceButton.setOnClickListener {
            startService(serviceIntent)
        }
    }

    private fun openDateTimeDialog() {
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val dialog = DatePickerDialog(
            this, this,
            calendar[Calendar.YEAR], calendar[Calendar.MONTH],
            calendar[Calendar.DAY_OF_MONTH]
        )

        val hour: Int = calendar.get(Calendar.HOUR_OF_DAY)
        val minute: Int = calendar.get(Calendar.MINUTE)

        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            this,
            OnTimeSetListener { timePicker, selectedHour, selectedMinute -> txtMsg.append("$selectedHour:$selectedMinute") },
            hour,
            minute,
            true
        ) //Yes 24 hour time

        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
        dialog.show()
    }

    private fun spinnerSet() {
        val spinnerArray = resources.getStringArray(R.array.spinner_array)
        ArrayAdapter.createFromResource(
            this,
            R.array.spinner_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            val spannable = SpannableStringBuilder("Text is spantastic!")
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    when {
                        spinnerArray[position] == "Normal" -> {
                            tvSpannableText.text = "Normal Text"

                        }
                        spinnerArray[position] == "Color Change" -> {
                            spannable.setSpan(
                                ForegroundColorSpan(Color.RED),
                                8, // start
                                12, // end
                                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
                            )
                            tvSpannableText.text = spannable
                        }
                        spinnerArray[position] == "Insert Text Between" -> {
                            spannable.setSpan(
                                StyleSpan(Typeface.BOLD),
                                10,
                                spannable.length,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            spannable.insert(12, "insert text")
                            tvSpannableText.text = spannable
                        }
                        spinnerArray[position] == "Underline Span" -> {
                            spannable.setSpan(
                                UnderlineSpan(),
                                10,
                                19,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            tvSpannableText.text = spannable
                        }
                        spinnerArray[position] == "Resize Text" -> {
                            spannable.setSpan(
                                RelativeSizeSpan(1.5f),
                                10,
                                24,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            tvSpannableText.text = spannable
                        }
                        spinnerArray[position] == "Highlighter Text" -> {
                            spannable.setSpan(
                                BackgroundColorSpan(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.colorYellow
                                    )
                                ), 17, 28, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            tvSpannableText.text = spannable
                        }
                        spinnerArray[position] == "Divide Text" -> {
                            spannable.setSpan(
                                QuoteSpan(
                                    ContextCompat.getColor(
                                        applicationContext,
                                        R.color.yellow
                                    )
                                ),
                                8,
                                spannable.length,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                            )
                            tvSpannableText.text = spannable
                        }
                    }
                }

            }
        }


    }

    private fun expandableTextView() {
        val text =
            "I tend to shy away from restaurant chains, but wherever I go, PF Chang&apos;s has solidly good food and, like Starbucks, they&apos;re reliable. We were staying in Boston for a week and after a long day and blah blah blah blah..."
        tvMore.text = text
    }


    private fun selectImage() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Choose your profile picture")
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            when {
                options[item] == "Take Photo" -> {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.CAMERA),
                            13
                        )

                    } else {
                        dispatchTakePictureIntent()
                    }
                }
                options[item] == "Choose from Gallery" -> {

                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            21
                        )
                    } else {
                        dispatchGalleryIntent()
                    }
                }
                options[item] == "Cancel" -> {
                    dialog.dismiss()
                }
            }
        })
        builder.show()
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile
                )
                mPhotoFile = photoFile
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(
                    takePictureIntent,
                    REQUEST_TAKE_PHOTO
                )
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.FROYO)
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val mFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(mFileName, ".jpg", storageDir)
    }


        private fun dispatchGalleryIntent() {
            val pickPhoto = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(
                pickPhoto,
                REQUEST_GALLERY_PHOTO
            )
        }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    mPhotoFile = mCompressor!!.compressToFile(mPhotoFile)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Glide.with(this)
                    .load(mPhotoFile)
                    .apply(
                        RequestOptions().centerCrop()
                            .circleCrop()
                            .placeholder(R.drawable.ic_launcher_background)
                    )
                    .into(imageChoose)
                ShardPref.savedata(this, mPhotoFile!!.toUri())
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                val selectedImage = data!!.data
                Glide.with(this)
                    .load(selectedImage)
                    .apply(
                        RequestOptions().centerCrop()
                            .circleCrop()
                            .placeholder(R.drawable.ic_launcher_background)
                    )
                    .into(imageChoose)
                if (selectedImage != null) {
                    ShardPref.savedata(this, selectedImage)
                }


            }
        }
    }


    private fun requestPermission() {
        requestPermissions(
            arrayOf(
                UserPermissionRequest.ACCESS_COARSE_LOCATION,
                UserPermissionRequest.ACCESS_FINE_LOCATION,
                UserPermissionRequest.RECEIVE_SMS,
                UserPermissionRequest.READ_PHONE_STATE,
                UserPermissionRequest.CALL_PHONE,
                UserPermissionRequest.READ_CALL_LOG
            ),
            REQUEST_CODE
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION)
        registerReceiver(wifiStateChanges, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(wifiStateChanges)
    }

    private val wifiStateChanges: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(
                WifiManager.EXTRA_WIFI_STATE,
                WifiManager.WIFI_STATE_UNKNOWN
            )) {
                WifiManager.WIFI_STATE_ENABLED -> {
                    wifiswitch.text = "Wifi on"
                    wifiswitch.isChecked = true
                }
                WifiManager.WIFI_STATE_UNKNOWN -> {
                    wifiswitch.text = "Wifi off"
                    wifiswitch.isChecked = false
                }
            }
        }
    }

    companion object {
        const val REQUEST_CODE = 0
        const val REQUEST_ENABLE_BT = 0
        const val REQUEST_TAKE_PHOTO = 1
        const val REQUEST_GALLERY_PHOTO = 2
    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        _birthYear = year
        _month = month
        _day = dayOfMonth
        updateDisplay()
    }

    private fun updateDisplay() {
        txtMsg.setText(
            StringBuilder() // Month is 0 based so add 1
                .append(_day).append("/").append(_month + 1).append("/").append(_birthYear)
                .append(" ")
        )
    }

    private fun detectFace(image: InputImage) {
        val option = FaceDetectorOptions.Builder()
            .setClassificationMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
            .setMinFaceSize(0.1F)
            .enableTracking()
            .build()


        val detector = FaceDetection.getClient(option)

        val result = detector.process(image)
            .addOnSuccessListener { faces ->

                for (face in faces) {
                    val bound = face.boundingBox
                    val rotY = face.headEulerAngleY
                    val rotZ = face.headEulerAngleZ


                    val leftEar = face.getLandmark(FaceLandmark.LEFT_EAR)
                    leftEar?.let {
                        val lefEarPosition = leftEar.position
                    }

                    if (face.smilingProbability != null) {
                        val smileProb = face.smilingProbability
                    }

                    if (face.trackingId != null) {
                        val id = face.trackingId
                    }
                }
            }
            .addOnFailureListener { e ->

            }


    }
}
