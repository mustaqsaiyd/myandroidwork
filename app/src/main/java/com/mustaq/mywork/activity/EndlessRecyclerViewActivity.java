package com.mustaq.mywork.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.mustaq.mywork.R;
import com.mustaq.mywork.adapter.AdapterEndless;
import com.mustaq.mywork.model.CoverPhotoModel;
import com.mustaq.mywork.retrofitClient.ServiceGenerator;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EndlessRecyclerViewActivity extends AppCompatActivity {

    String BASEURL = "https://fakerestapi.azurewebsites.net/api/";
    ServiceGenerator apiInterface;
    AdapterEndless adapterEndless;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endless_recycler_view);
        getApiCall();
        recyclerView=findViewById(R.id.rvEndless);

    }

    private void getApiCall() {
        Retrofit retrofit = null;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        apiInterface = retrofit.create(ServiceGenerator.class);

        Call<List<CoverPhotoModel>> call = apiInterface.getAllCoverPhoto();

        call.enqueue(new Callback<List<CoverPhotoModel>>() {
            @Override
            public void onResponse(Call<List<CoverPhotoModel>> call, Response<List<CoverPhotoModel>> response) {
                if (response.isSuccessful()) {
                    adapterEndless = new AdapterEndless(getApplicationContext(), response.body());
                    recyclerView.setHasFixedSize(true);
                    layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    recyclerView.setAdapter(adapterEndless);
                }
            }

            @Override
            public void onFailure(Call<List<CoverPhotoModel>> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t);

            }
        });


    }


}