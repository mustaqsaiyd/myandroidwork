package com.mustaq.mywork.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.mustaq.mywork.R;
import com.mustaq.mywork.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class AndroidViewPagerActivity extends AppCompatActivity {


    ViewPager2 viewPager2;
    TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_view_pager);

        viewPager2 = findViewById(R.id.viewPager2);
        tabLayout = findViewById(R.id.tab_layout);

        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");

        int[] imageArray = {
                R.drawable.thor,
                R.drawable.america,
                R.drawable.dtrange,
                R.drawable.marvel,
                R.drawable.ironman,};


        viewPagerAdapter = new ViewPagerAdapter(this, list, imageArray, viewPager2);

        viewPager2.setAdapter(viewPagerAdapter);


    }
}