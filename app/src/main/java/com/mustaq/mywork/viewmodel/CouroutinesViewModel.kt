package com.mustaq.mywork.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.mustaq.mywork.repository.MainRepository
import com.mustaq.mywork.model.Resource
import kotlinx.coroutines.Dispatchers

class CouroutinesViewModel(private val mainRepository: MainRepository) : ViewModel() {


    fun getUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getUsers()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

}
