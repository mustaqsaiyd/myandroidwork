package com.mustaq.roomexample.ui.mobileotp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MobileOtpViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is firebase mobile otp"
    }
    val text: LiveData<String> = _text
}