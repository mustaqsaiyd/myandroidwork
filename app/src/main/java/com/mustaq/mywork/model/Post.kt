package com.mustaq.mywork.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "student_table")
class Post(

    @PrimaryKey
    @ColumnInfo(name = "Post_Id")
    var id: Int,

    var userid: Int,
    @ColumnInfo(name = "Post_Title")
    var title: String,

    @ColumnInfo(name = "Post_Body")
    var body: String)