package com.mustaq.mywork.model

class Item {
        var owner: Owner? = null
        var is_accepted = false
        var score = 0
        var last_activity_date: Long = 0
        var creation_date: Long = 0
        var answer_id: Long = 0
        var question_id: Long = 0
    }