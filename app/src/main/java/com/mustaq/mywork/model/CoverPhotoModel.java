package com.mustaq.mywork.model;

public class CoverPhotoModel {

    /**
     * ID : 1
     * IDBook : 1
     * Url : https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350
     */

    private int ID;
    private int IDBook;
    private String Url;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getIDBook() {
        return IDBook;
    }

    public void setIDBook(int IDBook) {
        this.IDBook = IDBook;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String Url) {
        this.Url = Url;
    }
}
