package com.mustaq.mywork.model

data class EmployeeDataModel(
    val data: List<Data>,
    val status: String
)