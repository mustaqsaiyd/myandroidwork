package com.mustaq.mywork.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}