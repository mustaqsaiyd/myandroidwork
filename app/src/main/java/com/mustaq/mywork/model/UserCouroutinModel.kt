package com.mustaq.mywork.model


data class UserCouroutinModel(
    val image: String,
    val id: String,
    val title: String,
    val body: String
)