package com.mustaq.mywork.model;

public class BluetoothModel {
    private String bName;
    private String bAddress;
    private String bType;
    private String bBounds;
    private int bClass;

    public int getbClass() {
        return bClass;
    }

    public void setbClass(int bClass) {
        this.bClass = bClass;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public String getbAddress() {
        return bAddress;
    }

    public void setbAddress(String bAddress) {
        this.bAddress = bAddress;
    }

    public String getbType() {
        return bType;
    }

    public void setbType(String bType) {
        this.bType = bType;
    }

    public String getbBounds() {
        return bBounds;
    }

    public void setbBounds(String bBounds) {
        this.bBounds = bBounds;
    }
}
