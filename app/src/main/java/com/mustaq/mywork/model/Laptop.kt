package com.mustaq.mywork.model

class Laptop(var id: Int, var name: String, var ram: Int, var hardDisk: Int) :
    Comparable<Laptop> {
    override fun compareTo(other: Laptop): Int {
        return if (this.ram > other.ram) {
            1
        } else {
            -1
            }
    }

    override fun toString(): String {
        return "Laptop [id:- $id name:- $name ram:- $ram hardDisk:- $hardDisk]"
    }
}