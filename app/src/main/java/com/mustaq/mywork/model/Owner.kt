package com.mustaq.mywork.model

class Owner {
        var reputation = 0
        var user_id: Long = 0
        var user_type: String? = null
        var profile_image: String? = null
        var display_name: String? = null
        var link: String? = null
    }