package com.mustaq.mywork.model

data class Data(
    val employee_age: String,
    val employee_name: String,
    val employee_salary: String,
    val id: String,
    val profile_image: String
) : Comparable<Data> {
    override fun compareTo(other: Data): Int {
        return if (this.employee_age > other.employee_age) {
            1
        } else {
            -1
        }
    }
}