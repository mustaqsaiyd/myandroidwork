package com.mustaq.mywork.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.model.UserCouroutinModel
import kotlinx.android.synthetic.main.item_layout.view.*


class UserAdapter(private val userCouroutinModels: ArrayList<UserCouroutinModel>) :
    RecyclerView.Adapter<UserAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(userCouroutinModel: UserCouroutinModel) {

            itemView.apply {
                tvUserId.text = userCouroutinModel.id
                tvTitle.text = userCouroutinModel.title
                tvBody.text = userCouroutinModel.body
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        )

    override fun getItemCount(): Int = userCouroutinModels.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(userCouroutinModels[position])
    }

    fun addUsers(userCouroutinModels: List<UserCouroutinModel>) {
        this.userCouroutinModels.apply {
            clear()
            addAll(userCouroutinModels)
        }

    }
}