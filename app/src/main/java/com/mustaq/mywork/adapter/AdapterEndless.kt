package com.mustaq.mywork.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.AdapterEndless.EndlessHolder
import com.mustaq.mywork.model.CoverPhotoModel

class AdapterEndless(
    var mContext: Context,
    var coverPhotoModelArrayList: List<CoverPhotoModel>
) : RecyclerView.Adapter<EndlessHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EndlessHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.item_post, parent, false)
        return EndlessHolder(view)
    }

    override fun onBindViewHolder(holder: EndlessHolder, position: Int) {
        holder.textViewTitle.setText(coverPhotoModelArrayList[position].idBook)
    }

    override fun getItemCount(): Int {
        Log.e("TAG", "getItemCount: " + coverPhotoModelArrayList.size)
        return coverPhotoModelArrayList.size
    }

    inner class EndlessHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewTitle = itemView.findViewById<TextView>(R.id.textViewTitle)
    }


}

/**https://www.journaldev.com/24041/android-recyclerview-load-more-endless-scrolling**/