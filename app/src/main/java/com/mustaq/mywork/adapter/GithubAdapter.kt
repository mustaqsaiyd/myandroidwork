package com.mustaq.mywork.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.GithubAdapter.EmployeHolder
import com.mustaq.mywork.model.Item
import com.mustaq.mywork.retrofitClient.StackApiResponse
import retrofit2.Response

class GithubAdapter(
    val context: Context,
    val employeeList: Response<StackApiResponse?>
) : RecyclerView.Adapter<EmployeHolder>() {

    private val LIST_ITEM = 0
    private val GRID_ITEM = 1
    private var isSwitchView = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeHolder {
        val employeeView = if (viewType == LIST_ITEM) {
            LayoutInflater.from(context).inflate(R.layout.row_item_employe_list, parent, false)
        } else {
            LayoutInflater.from(context).inflate(R.layout.row_product_grid_item, null)
        }

        return EmployeHolder(employeeView)

    }

    override fun onBindViewHolder(holder: EmployeHolder, position: Int) {
        holder.bind(employeeList.body()!!.items)
    }

    override fun getItemCount(): Int {
        return employeeList.body()!!.items!!.size
    }

    inner class EmployeHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var tvEmployeeId = itemView.findViewById<TextView>(R.id.tvEmployeeId)
        var tvEmployeeName = itemView.findViewById<TextView>(R.id.tvEmployeeName)
        var tvEmployeeSalary = itemView.findViewById<TextView>(R.id.tvEmployeeSalary)
        var tvEmployeeAge = itemView.findViewById<TextView>(R.id.tvEmployeeAge)
        var tvEmployeeImage = itemView.findViewById<ImageView>(R.id.imageView2)

        @SuppressLint("SetTextI18n")
        fun bind(items: List<Item>?) {
            tvEmployeeId.text = "Id:-" + items!![position].owner!!.user_id
            tvEmployeeName.text = "Name:-" + items[position].owner!!.display_name
            tvEmployeeAge.text = "Type:-" + items[position].owner!!.user_type
            tvEmployeeSalary.text = "Reputation:-." + items[position].owner!!.reputation
            Glide
                .with(context)
                .load(items[position].owner!!.profile_image)
                .centerCrop()
                .placeholder(R.drawable.no_image_available)
                .into(tvEmployeeImage);

        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isSwitchView) {
            LIST_ITEM
        } else {
            GRID_ITEM
        }
    }

    fun toggleItemViewType(): Boolean {
        isSwitchView = !isSwitchView
        return isSwitchView
    }

}