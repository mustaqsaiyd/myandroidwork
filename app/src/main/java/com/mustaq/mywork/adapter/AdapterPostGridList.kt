package com.mustaq.mywork.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R

class AdapterPostGridList(val user: ArrayList<String>) :
    RecyclerView.Adapter<AdapterPostGridList.ViewHolder>() {

    private val LIST_ITEM = 0
    private val GRID_ITEM = 1
    private var isSwitchView = true

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = if (viewType == LIST_ITEM) {
            LayoutInflater.from(parent.context).inflate(R.layout.row_product_list_items, null)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.row_product_grid_item, null)
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        holder.bindData(user)
    }

    override fun getItemCount(): Int {
        return user.size
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val tvUserName = itemView.findViewById<AppCompatTextView>(R.id.tvName)
        fun bindData(user: ArrayList<String>) {
            tvUserName.text = user[position]
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isSwitchView) {
            LIST_ITEM
        } else {
            GRID_ITEM
        }
    }

    fun toggleItenViewType(): Boolean {
        isSwitchView = !isSwitchView
        return isSwitchView
    }
}