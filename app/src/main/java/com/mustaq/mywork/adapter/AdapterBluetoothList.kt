package com.mustaq.mywork.adapter

import android.bluetooth.BluetoothClass
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.model.BluetoothModel
import com.mustaq.mywork.mainapp.MainApplication

class AdapterBluetoothList(
    var context: Context,
    var list: ArrayList<BluetoothModel>
) : RecyclerView.Adapter<AdapterBluetoothList.BluetoothHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BluetoothHolder {
        val rootView =
            LayoutInflater.from(context).inflate(R.layout.row_item_bluetooth, parent, false)
        return BluetoothHolder(rootView)
    }

    override fun onBindViewHolder(holder: BluetoothHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
    }

    inner class BluetoothHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var bluetoothName: AppCompatTextView = itemView.findViewById(R.id.tvName)
        var imgBluetooth:AppCompatImageView=itemView.findViewById(R.id.imgBluetooth)
        fun bind(position: Int) {

            if (list[position].getbClass() == BluetoothClass.Device.COMPUTER_DESKTOP) {
                imgBluetooth.background=ContextCompat.getDrawable(MainApplication.getCurrentContext(),R.drawable.ic_bluetooth)
            }
            bluetoothName.text = list[position].getbName()
        }
    }

    companion object {
        const val TAG = "Adapter"
    }
}