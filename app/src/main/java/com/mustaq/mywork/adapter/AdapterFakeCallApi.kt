package com.mustaq.mywork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.AdapterFakeCallApi.FackHolder
import com.mustaq.mywork.viewmodel.FakeApiModel

class AdapterFakeCallApi(
    val context: Context,
    val dataList: List<FakeApiModel.DataBean>?
) : RecyclerView.Adapter<FackHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FackHolder {
        return FackHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_face_api_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: FackHolder, position: Int) {
        holder.bind(dataList)
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    inner class FackHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        val id = itemView.findViewById<AppCompatTextView>(R.id.tvFId)
        val firstName = itemView.findViewById<AppCompatTextView>(R.id.tvFirstname)
        val lastName = itemView.findViewById<AppCompatTextView>(R.id.tvLastName)
        val email = itemView.findViewById<AppCompatTextView>(R.id.tvFEmail)
        val imgUser = itemView.findViewById<AppCompatImageView>(R.id.imgUser)
        fun bind(dataList: List<FakeApiModel.DataBean>?) {

            id.text = dataList!![position].first_name
            firstName.text = dataList[position].last_name
            lastName.text = dataList[position].last_name
            email.text = dataList[position].email
            Glide.with(context).load(dataList[position].avatar).into(imgUser)
        }
    }
}