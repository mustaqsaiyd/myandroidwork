package com.mustaq.mywork.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mustaq.mywork.model.EmployeeModel;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<EmployeeModel.DataBean> beanArrayList;
    private LayoutInflater layoutInflater;

    public RecyclerViewAdapter(ArrayList<EmployeeModel.DataBean> beanArrayList) {
        this.beanArrayList = beanArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        com.mustaq.mywork.databinding.RowItemDataBindingBinding rowItemDataBindingBinding
                = com.mustaq.mywork.databinding.RowItemDataBindingBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(rowItemDataBindingBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EmployeeModel.DataBean model = beanArrayList.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return beanArrayList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private com.mustaq.mywork.databinding.RowItemDataBindingBinding rowItemDataBindingBinding;

        ViewHolder(com.mustaq.mywork.databinding.RowItemDataBindingBinding itemDataBindingBinding) {
            super(itemDataBindingBinding.getRoot());
            this.rowItemDataBindingBinding = itemDataBindingBinding;
        }

        void bind(EmployeeModel.DataBean employeeModel) {
            this.rowItemDataBindingBinding.setEmployeeModel(employeeModel);
        }

    }
}
