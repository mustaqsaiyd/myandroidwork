package com.mustaq.mywork.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.adapter.FilterAdapter.FilterHolder

class FilterAdapter(
    val context: Context,
    private val map: MutableMap<String, String>
) : RecyclerView.Adapter<FilterHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.filter_layout, parent, false)
        return FilterHolder(view)
    }

    override fun onBindViewHolder(holder: FilterHolder, position: Int) {
        holder.bind(map[(map.keys.toTypedArray()[position])])
    }
    override fun getItemCount(): Int {
        return map.size
    }

    inner class FilterHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView){

        fun bind(colorList: String?) {
            val bgDrawable = ContextCompat.getDrawable(context, R.drawable.filter_color_circle)
            bgDrawable!!.setColorFilter(Color.parseColor(colorList), PorterDuff.Mode.ADD)
            itemView.setBackgroundDrawable(bgDrawable)
        }
    }
}