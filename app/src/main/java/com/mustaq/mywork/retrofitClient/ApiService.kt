package com.mustaq.mywork.retrofitClient

import com.mustaq.mywork.model.UserCouroutinModel
import retrofit2.http.GET

interface ApiService {

    @GET("posts")
    suspend fun getUsers(): List<UserCouroutinModel>

}