package com.mustaq.mywork.retrofitClient

import com.mustaq.mywork.model.Item


class StackApiResponse {
    var items: List<Item>? = null
    var has_more = false
    var quota_max = 0
    var quota_remaining = 0

}
