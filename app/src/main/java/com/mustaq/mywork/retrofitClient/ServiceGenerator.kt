package com.mustaq.mywork.retrofitClient

import com.mustaq.mywork.model.CoverPhotoModel
import com.mustaq.mywork.model.EmployeeModel
import com.mustaq.mywork.viewmodel.FakeApiModel

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ServiceGenerator {
    @GET(POST)
    fun getUserList(): Call<EmployeeModel>

    @GET("answers")
    fun getAnswers(
        @Query("page") page: Int,
        @Query("pagesize") pagesize: Int,
        @Query("site") site: String?
    ): Call<StackApiResponse?>?


    @GET("users")
    fun getFakeCall():Call<List<FakeApiModel.DataBean>>

    @GET("CoverPhotos")
    fun getAllCoverPhoto():Call<List<CoverPhotoModel>>
}
//push in bucket