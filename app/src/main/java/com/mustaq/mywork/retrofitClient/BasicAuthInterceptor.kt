package com.attune.zamy.retrofitClient


import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class BasicAuthInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val authenticatedRequest = request.newBuilder()
            .header("Authorization", "Basic YWRtaW46MTIzNA==")
            .header("HOP-API-KEY","CODEX@123").build()

        return chain.proceed(authenticatedRequest)
    }
}