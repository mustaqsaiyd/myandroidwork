package com.mustaq.mywork.retrofitClient

import com.mustaq.mywork.model.Post
import io.reactivex.Observable
import retrofit2.http.GET

interface ImApi {
    @get:GET("posts")
    val post: Observable<List<Post?>?>?
}