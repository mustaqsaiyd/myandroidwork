package com.mustaq.mywork.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mustaq.mywork.retrofitClient.ApiHelper
import com.mustaq.mywork.repository.MainRepository
import com.mustaq.mywork.viewmodel.CouroutinesViewModel


class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CouroutinesViewModel::class.java)) {
            return CouroutinesViewModel(
                MainRepository(apiHelper)
            ) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}