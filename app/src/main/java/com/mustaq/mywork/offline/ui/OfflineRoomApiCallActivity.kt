package com.mustaq.mywork.offline.ui

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.mywork.R
import com.mustaq.mywork.utils.centerToast
import com.mustaq.mywork.offline.adapter.ActorAdapter
import com.mustaq.mywork.offline.api.ActorApiService
import com.mustaq.mywork.offline.model.Actor
import com.mustaq.mywork.offline.onActorItemClick
import com.mustaq.mywork.offline.repository.ActorRepository
import com.mustaq.mywork.offline.viewmodel.ActorViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.alert_edit_delete.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class OfflineRoomApiCallActivity : AppCompatActivity(), onActorItemClick {


    lateinit var actorViewModel: ActorViewModel
    lateinit var getActorlist: List<Actor>
    lateinit var actorAdapter: ActorAdapter
    lateinit var recyclerView: RecyclerView
    lateinit var repository: ActorRepository
    lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offline_room_api_call)
        repository = ActorRepository(application)
        getActorlist = ArrayList()
        recyclerView = findViewById(R.id.rvActor)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        actorViewModel = ViewModelProviders.of(this).get(ActorViewModel::class.java)
        actorAdapter = ActorAdapter(this, getActorlist, this)

        /*actorViewModel.getAllActor.observe(this, Observer {
            actorAdapter.getAllActors(it)
            recyclerView.adapter = ActorAdapter(this, it, this)
            Log.d("TAG", "" + it)
        })*/
        compositeDisposable = CompositeDisposable()

        actorViewModel.getAllActor
            .observe(this,
                Observer<List<Actor>> { actorList ->
                    //actorAdapter.getAllActors(actorList)
                    recyclerView.adapter = ActorAdapter(this, actorList, this)
                })

        getAi()
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()

    }

    private fun getAi() {

        val retrofit = Retrofit.Builder()
            .baseUrl("http://www.codingwithjks.tech/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiService = retrofit.create(ActorApiService::class.java)

        val call = apiService.getActor()
        call.enqueue(object : retrofit2.Callback<List<Actor>> {
            override fun onFailure(call: Call<List<Actor>>, t: Throwable) {
                centerToast(
                    this@OfflineRoomApiCallActivity,
                    "Something Went Wrong"
                )
                Log.d("TAG", "onFailure: $t")
            }

            override fun onResponse(call: Call<List<Actor>>, response: Response<List<Actor>>) {
                if (response.isSuccessful) {

                    if (response.body()!!.isNotEmpty()) {
                        repository.insert(response.body())
                    } else {
                        centerToast(
                            this@OfflineRoomApiCallActivity,
                            "Data Not Available"
                        )
                    }
                } else {
                    centerToast(
                        this@OfflineRoomApiCallActivity,
                        "Something went wrong"
                    )
                }
            }

        })
    }

    override fun actorClick(id: Int) {
        val myDialog = Dialog(this)
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        myDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog.setContentView(R.layout.alert_edit_delete)
        myDialog.show()

        myDialog.btnEdit.setOnClickListener {
            centerToast(this, "Edit")
        }
        myDialog.btnDelete.setOnClickListener {
            centerToast(this, "Delete")
        }
    }
}