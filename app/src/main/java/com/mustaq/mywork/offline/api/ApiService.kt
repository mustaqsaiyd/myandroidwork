package com.mustaq.mywork.offline.api

import com.mustaq.mywork.offline.model.Actor
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("data.php")
    fun getActor(): Call<List<Actor>>
}