package com.mustaq.mywork.services

import android.app.IntentService
import android.content.Intent
import android.util.Log

class MyIntentService : IntentService("MyIntentService") {
    override fun onHandleIntent(intent: Intent?) {

        for (i in 0..5) {
            Log.d("MyIntentService", "${intent!!.getIntExtra("type", 0)}" + " Count : $i")
            Thread.sleep(1000)
        }
    }
}
