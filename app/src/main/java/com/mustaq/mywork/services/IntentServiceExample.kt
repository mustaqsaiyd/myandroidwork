package com.mustaq.mywork.services

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mustaq.mywork.R

const val Type1=1
const val Type2=2

class IntentServiceExample : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_service_example)
        val intent=Intent(this, MyIntentService::class.java)
        intent.putExtra("type", Type1)
        startService(intent)
        val intent1=Intent(this, MyIntentService::class.java)
        intent.putExtra("type", Type2)
        startService(intent1)
    }
}