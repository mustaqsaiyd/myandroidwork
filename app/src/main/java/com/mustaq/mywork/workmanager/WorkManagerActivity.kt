package com.mustaq.mywork.workmanager

import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.mustaq.mywork.R
import kotlinx.android.synthetic.main.activity_work_manager.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.jar.Manifest


class WorkManagerActivity : AppCompatActivity() {


    val workRequest = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_manager)

        buttonEnqueue.setOnClickListener {
            WorkManager.getInstance().enqueue(workRequest)
        }

    }
}

