package com.mustaq.mywork.helper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class RectOverlay extends GraphicOverlay.Graphic {

    private int mRectColor = Color.GREEN;
    private float mStockWidth = 4.0f;
    private Paint mReactPaint;
    private GraphicOverlay graphicOverlay;
    private Rect mRect;

    public RectOverlay(GraphicOverlay overlay, Rect rect) {
        super(overlay);
        mReactPaint = new Paint();
        mReactPaint.setColor(mRectColor);
        mReactPaint.setStyle(Paint.Style.STROKE);
        mReactPaint.setStrokeWidth(mStockWidth);
        this.graphicOverlay = overlay;
        this.mRect = rect;
        postInvalidate();
    }

    @Override
    public void draw(Canvas canvas) {
        RectF rectF = new RectF(mRect);
        rectF.left = translateX(rectF.left);
        rectF.right = translateX(rectF.right);
        rectF.top = translateX(rectF.top);
        rectF.bottom = translateX(rectF.bottom);
        canvas.drawRect(rectF, mReactPaint);

    }
}
