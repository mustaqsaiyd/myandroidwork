package com.mustaq.mywork.helper

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.core.content.PermissionChecker

object UserPermissionRequest {
    private const val PTAG = "TAG"

    // Calendar group.
    const val READ_CALENDAR = Manifest.permission.READ_CALENDAR
    const val WRITE_CALENDAR = Manifest.permission.WRITE_CALENDAR

    // Camera group.
    const val CAMERA = Manifest.permission.CAMERA

    // Contacts group.
    const val READ_CONTACTS = Manifest.permission.READ_CONTACTS
    const val WRITE_CONTACTS = Manifest.permission.WRITE_CONTACTS

    // Location group.
    const val ACCESS_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    const val ACCESS_COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION

    // Microphone group.
    const val RECORD_AUDIO = Manifest.permission.RECORD_AUDIO

    // Phone group.
    const val READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE
    const val CALL_PHONE = Manifest.permission.CALL_PHONE
    const val READ_CALL_LOG = Manifest.permission.READ_CALL_LOG
    const val WRITE_CALL_LOG = Manifest.permission.WRITE_CALL_LOG
    const val ADD_VOICEMAIL = Manifest.permission.ADD_VOICEMAIL
    const val USE_SIP = Manifest.permission.USE_SIP
    const val PROCESS_OUTGOING_CALLS = Manifest.permission.PROCESS_OUTGOING_CALLS

    // Sensors group.
    const val BODY_SENSORS = Manifest.permission.BODY_SENSORS
    const val USE_FINGERPRINT = Manifest.permission.USE_FINGERPRINT

    // SMS group.
    const val SEND_SMS = Manifest.permission.SEND_SMS
    const val RECEIVE_SMS = Manifest.permission.RECEIVE_SMS
    const val READ_SMS = Manifest.permission.READ_SMS
    const val RECEIVE_WAP_PUSH = Manifest.permission.RECEIVE_WAP_PUSH
    const val RECEIVE_MMS = Manifest.permission.RECEIVE_MMS
    const val READ_CELL_BROADCASTS = "android.permission.READ_CELL_BROADCASTS"


    fun asArray(vararg permissions: String): Array<String?> {
        require(permissions.isNotEmpty()) { "There is no given permission" }
        val dest = arrayOfNulls<String>(permissions.size)
        var i = 0
        val len = permissions.size
        while (i < len) {
            dest[i] = permissions[i]
            i++
        }
        return dest
    }

    /**
     * Check that given permission have been granted.
     */
    fun hasGranted(grantResult: Int): Boolean {
        return grantResult == PermissionChecker.PERMISSION_GRANTED
    }

    fun hasGranted(grantResults: IntArray): Boolean {
        for (result in grantResults) {
            if (!hasGranted(result)) {
                return false
            }
        }
        return true
    }

    /**
     * Returns true if the Context has access to a given permission.
     * Always returns true on platforms below M.
     */
    fun hasSelfPermission(context: Context, permission: String): Boolean {
        return if (isPermisssion) {
            permissionHasGranted(context, permission)
        } else true
    }

    /**
     * Returns true if the Context has access to all given permissions.
     * Always returns true on platforms below M.
     */
    fun hasSelfPermissions(
        context: Context,
        permissions: Array<String>
    ): Boolean {
        if (!isPermisssion) {
            return true
        }
        for (permission in permissions) {
            if (!permissionHasGranted(context, permission)) {
                return false
            }
        }
        return true
    }

    /**
     * Requests permissions to be granted to this application.
     */
    fun requestAllPermissions(
        activity: Activity,
        permissions: Array<String>,
        requestCode: Int
    ) {
        if (isPermisssion) {
            internalRequestPermissions(activity, permissions, requestCode)
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun internalRequestPermissions(
        activity: Activity?,
        permissions: Array<String>,
        requestCode: Int
    ) {
        requireNotNull(activity) { "Given activity is null." }
        activity.requestPermissions(permissions, requestCode)
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun permissionHasGranted(
        context: Context,
        permission: String
    ): Boolean {
        return hasGranted(context.checkSelfPermission(permission))
    }

    val isPermisssion: Boolean get() = PTAG == Build.VERSION.CODENAME
}