package com.mustaq.mywork.repository

import com.mustaq.mywork.retrofitClient.ApiHelper

class MainRepository(private val apiHelper: ApiHelper) {

    suspend fun getUsers() = apiHelper.getUsers()

}