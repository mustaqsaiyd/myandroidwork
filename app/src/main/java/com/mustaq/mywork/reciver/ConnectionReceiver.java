package com.mustaq.mywork.reciver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class ConnectionReceiver extends BroadcastReceiver {
    String TAG="Connection Receiver";
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive: "+intent.getAction());

        if(intent.getAction().equals("com.example.broadcastreciver.SOME_ACTION"))
        {
            Toast.makeText(context, "SOME ACTION Recevied ", Toast.LENGTH_SHORT).show();
        }
        else {
            ConnectivityManager connectivityManager =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo=connectivityManager.getActiveNetworkInfo();
            boolean isConnected=activeNetworkInfo!=null && activeNetworkInfo.isConnectedOrConnecting();
            if (isConnected){
                try {
                    Toast.makeText(context, "Network Is Connected", Toast.LENGTH_SHORT).show();

                }catch (Exception e)
                {
                    Log.e(TAG, "onReceive: " );
                }
            }else {
                Toast.makeText(context, "Network is Changed or Reconnected", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
